# React cursor detection

[![NPM-VERSION](https://img.shields.io/npm/v/react-cursor-detection.svg)](https://www.npmjs.com/package/react-cursor-detection) [![LICENSE](https://img.shields.io/npm/l/react-cursor-detection.svg?color=green)](https://www.npmjs.com/package/react-cursor-detection) [![coverage report](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/badges/master/coverage.svg)](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/commits/master)

A react component that detects the coordinates of the mouse relative to the component. An example can be found [here](https://straightouttacrompton.gitlab.io/react-cursor-detection/).

## Install

```sh
npm i react-cursor-detection
```

## Usage

### TypeScript

```tsx
// App.tsx
import React from "react";
import ReactCursorDetection from "react-cursor-detection";
import ExampleUsage from "ExampleUsage";

export default function App() {
    return <ReactCursorDetection>{(props) => <ExampleUsage {...props} />}</ReactCursorDetection>;
}
```

```tsx
// ExampleUsage.tsx
import React from "react";
import { ICursorDetectionPassThroughProps } from "react-cursor-detection";

interface IProps extends ICursorDetectionPassThroughProps {}

export default function ExampleUsage(props: IProps) {
    const { isActive, position } = props;
    const { x, y } = position;

    return (
        <div>
            <p>isActive: {isActive ? "true" : "false"}</p>
            <p>
                x: {x}, y: {y}
            </p>
        </div>
    );
}
```

## Development

### Setup

#### Module

Install dependencies:

```sh
npm install
```

This should install dependencies for both the module and the example project **_(currently only works in linux)_**. If the example project dependencies fail, then please follow the [example cra dependency installation instructions](#example-create-react-app-project).

To run in watch mode:

```sh
npm start
```

To build:

```sh
npm run build
```

#### Example create-react-app project

First, cd into the example directory:

```sh
cd example
```

Install dependencies:

```sh
npm install
```

To run development server:

```sh
npm start
```

To build:

```sh
npm run build
```

### Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md).

## Credit

This project is based on [this solution](https://github.com/ethanselzer/react-cursor-position), but has now diverted significantly.
