## [1.0.7](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/compare/v1.0.6...v1.0.7) (2020-10-24)


### Bug Fixes

* example/package.json & example/package-lock.json to reduce vulnerabilities ([0a1a84b](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/commit/0a1a84b2ea20093a778d606e5ed5e8d7f5fbba2e))

## [1.0.6](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/compare/v1.0.5...v1.0.6) (2020-04-21)


### Bug Fixes

* removed post install ([15f033c](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/commit/15f033cb5f536143b67afc3f74ba65bbc9563ddd))

## [1.0.5](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/compare/v1.0.4...v1.0.5) (2020-04-02)

## [1.0.4](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/compare/v1.0.3...v1.0.4) (2020-03-31)

## [1.0.3](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/compare/v1.0.2...v1.0.3) (2020-03-31)

## [1.0.2](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/compare/v1.0.1...v1.0.2) (2020-03-31)

## [1.0.1](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/compare/v1.0.0...v1.0.1) (2020-03-31)

# 1.0.0 (2020-03-31)


### Bug Fixes

* added gpl3 license ([9c76438](https://gitlab.com/StraightOuttaCrompton/react-cursor-detection/commit/9c764387830072e1e1a3eadfd7b35068c5127049))
