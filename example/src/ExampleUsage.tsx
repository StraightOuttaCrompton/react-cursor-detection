import React from "react";
import { ICursorDetectionPassThroughProps } from "react-cursor-detection";
import "ExampleUsage.css";

interface IProps extends ICursorDetectionPassThroughProps {}

export default function ExampleUsage(props: IProps) {
    const { isActive, position } = props;
    const { x, y } = position;

    return (
        <div className="ExampleUsage">
            <p>isActive: {isActive ? "true" : "false"}</p>
            <p>
                x: {x}, y: {y}
            </p>
        </div>
    );
}
