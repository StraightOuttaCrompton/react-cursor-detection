import React from "react";
import "App.css";
import ReactCursorDetection from "react-cursor-detection";
import ExampleUsage from "ExampleUsage";

export default function App() {
    return (
        <>
            <h1 className="Title">React cursor detection example</h1>
            <div className="ReactCursorDetectionContainer">
                <ReactCursorDetection className="ReactCursorDetection">
                    {(props) => <ExampleUsage {...props} />}
                </ReactCursorDetection>
            </div>
        </>
    );
}
