import React, { ReactNode, useState, useEffect, useRef, HTMLAttributes } from "react";
import noop from "./utils/noop";
import ICoord from "./models/ICoord";
import HTMLElementCursorWrapper from "./elementWrappers/HTMLElementCursorWrapper";

export interface ICursorDetectionPassThroughProps {
    position: ICoord;
    isActive: boolean;
}

interface IProps<T> extends HTMLAttributes<T> {
    children: (props: ICursorDetectionPassThroughProps) => ReactNode;
    onIsActiveChange?: (isActive: boolean) => void;
    onPositionChange?: (position: ICoord) => void;
}

type RootHtmlElement = HTMLDivElement;

export default function ReactCursorDetection(props: IProps<RootHtmlElement>) {
    const { children, onIsActiveChange = noop, onPositionChange = noop, ...rest } = props;

    const [position, setPosition] = useState<ICoord>({
        x: 0,
        y: 0,
    });
    const [isActive, setIsActive] = useState<boolean>(false);

    const [core, setCore] = useState<HTMLElementCursorWrapper<RootHtmlElement>>();
    const rootEl = useRef<RootHtmlElement>(null);

    useEffect(() => {
        if (rootEl.current !== null) {
            const elementRelativeCursorPosition = new HTMLElementCursorWrapper(rootEl.current);
            setCore(elementRelativeCursorPosition);
        }
    }, [setCore]);

    const onMouseEnter = (event: React.MouseEvent<RootHtmlElement, MouseEvent>) => {
        positionChange(event);

        setIsActive(true);
        onIsActiveChange(true);
    };

    const onMouseMove = (event: React.MouseEvent<RootHtmlElement, MouseEvent>) => {
        positionChange(event);
    };

    const positionChange = (event: React.MouseEvent) => {
        if (typeof core === "undefined") return;

        const _postion = core.getRelativeCursorPosition(event.nativeEvent);

        setPosition(_postion);
        onPositionChange(_postion);
    };

    const onMouseLeave = () => {
        setIsActive(false);
        onIsActiveChange(false);
    };

    return (
        <div
            onMouseEnter={onMouseEnter}
            onMouseMove={onMouseMove}
            onMouseLeave={onMouseLeave}
            ref={rootEl}
            {...rest}
        >
            {children({
                position,
                isActive,
            })}
        </div>
    );
}
