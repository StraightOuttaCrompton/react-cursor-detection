import ICoord from "../models/ICoord";
import HTMLElementWrapper from "./HTMLElementWrapper";

export default class HTMLElementCursorWrapper<T extends HTMLElement> extends HTMLElementWrapper<T> {
    public getRelativeCursorPosition(event: MouseEvent): ICoord {
        const elementRect = this._element.getBoundingClientRect();

        return {
            x: Math.round(event.pageX - elementRect.left),
            y: Math.round(event.pageY - elementRect.top),
        };
    }
}
