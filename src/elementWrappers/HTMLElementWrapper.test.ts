import HTMLElementWrapper from "./HTMLElementWrapper";

class HTMLElementWrapperTester<T extends HTMLElement> extends HTMLElementWrapper<T> {
    public testGlobalRootElement() {
        return this.globalRootElement;
    }
}

const parentId = "parent";
const childId = "child";

const parentDiv = document.createElement("div");
const childDiv = document.createElement("div");

parentDiv.id = parentId;
childDiv.id = childId;

parentDiv.append(childDiv);
document.body.append(parentDiv);

describe("HTMLElementWrapper", () => {
    describe("globalRootElement", () => {
        it("should get the root element of the document", () => {
            const elementWrapperTester = new HTMLElementWrapperTester(childDiv);

            const result = elementWrapperTester.testGlobalRootElement();

            expect(result.isEqualNode(document.body.parentElement)).toBeTruthy();
        });
    });
});
