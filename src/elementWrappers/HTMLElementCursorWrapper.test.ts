import HTMLElementCursorWrapper from "./HTMLElementCursorWrapper";

const parentId = "parent";
const childId = "child";

const parentDiv = document.createElement("div");
const childDiv = document.createElement("div");

parentDiv.id = parentId;
childDiv.id = childId;

const boundingClientRect = {
    top: 8,
    left: 4,
    right: 12,
    bottom: 16,
};

//@ts-ignore
childDiv.getBoundingClientRect = jest.fn(() => ({
    top: boundingClientRect.top,
    right: boundingClientRect.right,
    bottom: boundingClientRect.bottom,
    left: boundingClientRect.left,
}));

parentDiv.append(childDiv);
document.body.append(parentDiv);

describe("HTMLElementCursorWrapper", () => {
    describe("getRelativeCursorPosition", () => {
        it("should get cursor position relative to the elements bounding rect", () => {
            const mouseEvent = new MouseEvent("mousemove");

            const mockMouseEvent: MouseEvent = {
                ...mouseEvent,
                pageX: (boundingClientRect.left + boundingClientRect.right) / 2,
                pageY: (boundingClientRect.top + boundingClientRect.bottom) / 2,
            };

            const elementCursorWrapper = new HTMLElementCursorWrapper(childDiv);

            const result = elementCursorWrapper.getRelativeCursorPosition(mockMouseEvent);

            expect(result).toStrictEqual({
                x: (boundingClientRect.right - boundingClientRect.left) / 2,
                y: (boundingClientRect.bottom - boundingClientRect.top) / 2,
            });
        });
    });
});

// https://marcgrabanski.com/simulating-mouse-click-events-in-javascript
//https://stackoverflow.com/questions/3277369/how-to-simulate-a-click-by-using-x-y-coordinates-in-javascript
