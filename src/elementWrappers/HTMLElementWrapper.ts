export default class HTMLElementWrapper<T extends HTMLElement> {
    protected readonly _element: T;
    private _globalRootElement?: HTMLElement;

    constructor(element: T) {
        this._element = element;
    }

    protected get globalRootElement(): HTMLElement {
        if (typeof this._globalRootElement === "undefined")
            this._globalRootElement = this.getGlobalRootElement(this._element);

        return this._globalRootElement;
    }

    private getGlobalRootElement(el: HTMLElement): HTMLElement {
        if (el.parentElement) {
            return this.getGlobalRootElement(el.parentElement);
        }
        return el;
    }
}
